install:
	pip install -U pip
	pip install -U setuptools
	pip install pipenv


start-runner:
	gitlab-ci-multi-runner run --working-directory /home/gitlab-runner --config /etc/gitlab-runner/config.toml --service gitlab-runner --syslog --user gitlab-runner