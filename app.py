from flask import Flask, request

app = Flask(__name__)


@app.route("/", methods=["GET"])
def helloWorld():
    return {"status": "Runner Machine"}


if __name__ == '__main__':
    app.run(host="localhost", port=8090, debug=True)
